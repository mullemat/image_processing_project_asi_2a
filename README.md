# Image processing course ASI 2A - Project

## 1. Introduction

As a final exam, this project aims to evaluate 2A ASI students. 

Students will work in groups of 3 or 4, and each group must submit its work (report and code) on Chamilo by **Tuesday, June 6**.

## 2. Presentation

The project is a simple application of *image tiling*. We aim to reconstruct an image with little images, which we call "patches". We can see an example below: 

![alt text](readme_imgs/img0.png)

But this is a very long and hard task to do by hand. We want to do this task automatically. 

We propose you to reconstruct 4 images (you can find them in the *"images"* folder of this project). These images are from the open dataset of the **National Gallery of Art**, USA, the link to the dataset is the [following](https://github.com/NationalGalleryOfArt/opendata). To reconstruct these images we provide you a database of patches (stored as a single file called *"patches.npy"* in the root folder of this project). Those patches come from the ImageNet database, database used to train a lot of deep/machine learing algorithms. You can find the documentation about this database [here](https://www.image-net.org/index.php).

## 3. How to proceed?

All of the images to reconstruct have the same size (1024x1024) and are RGB. The patches to use are all same sized and squared (32x32) and are RGB too. To achieve this project you have to: 

- **a.** Extract all possible pieces (32x32 patches) from the image to reconstruct, let's call a piece from the image to reconstruct a query (*q in the figure below*).
- **b.** Find for each query the patch (*p in the figure below*) that best fit the query with the help of an image processing based measure **that you will build (feel free to search on the internet!)**. The patches from the database must be used only **one time**.
- **c.** Reconstruct the image with the found patches.

![alt text](readme_imgs/img1.png)

## 4. Some hints to start

We want you to find a metric that allows to find the patch that best fit a given query. Remember how to compute the distance between two images (BE2, BE3).

You can look further and find among these references some other metrics that you can implement to have better results:  

- Histogram in image processing [Wikipedia](https://fr.wikipedia.org/wiki/Histogramme_(imagerie_num%C3%A9rique))
- Mean Square Error [Wikipedia](https://en.wikipedia.org/wiki/Mean_squared_error)
- PSNR [Wikipedia](https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio)
- Other measure? 

*The time of computations might be very long depending of your machine. To verify that your method works, try to work with a few patches number (look at the source code and the provided documentation with the help() function).*

*Don't forget: images are RGB! The three channels are important!*

## 5. Project repository

The project has the following organization: 

~~~text
image_processing_project/
├─.gitignore                   # Ignore this
├─images/                      # All images you need to reconstruct
│ ├─img_1.png
│ ├─img_2.png
│ ├─img_3.png
│ └─img_4.png
├─main.ipynb                   # The notebook to run all of your computations
├─output/                      # The output folder where you can save your reconstruction (ignore .gitkeep)
│ └─.gitkeep
├─patches.npy                  # All of the patches from ImageNet saved in one file
├─readme.md                    # ReadMe, contains all information about the project
├─readme_imgs/                 # Images for the ReadMe, ignore it
│ ├─img0.jpg
│ └─img1.png
├─requirements.txt             # Requirement file for packages installation
└─src/                         # All of the source files for the project, your source file must appear here!
  ├─demo_reconstructions.py
  ├─reconstruct.py
  └─utils.py
  ~~~

## 6. Instructions: 

Each group will submit its work in Chamilo, inside the work **Image_processing_projects_2023**. It must contain:

- A report **as a pdf file** with the name **name1_name2_name3.pdf**.
- Your code as a archive (all of the provided project folder named image_processing_folder **except the `patches.npy` file**).

The code and the report must be **written in English**.

### 6.1. The report:

Your report **must be a pdf file** written in English. In this file you are asked to explain:

1. The problem statement, show us that you've understood the project.
2. The solution that you've chosen, explaining the theory.
3. With tools that **you've built** show us that your solution is relevant and working (*hint: maybe try to reconstruct an image with a patch database built with the image itself...*).
4. Results, give us your results of the images `img_1`, `img_2`, `img_3`, `img_4`.
5. Conclusion, take a step back about your results. What can be improved?

### 6.2. The code:

- Your code must compile and be operational.
- In the notebook, feel free to add cells to show us how your code works and to show us the tools you've developped to evaluate your work.
- Comment your code when needed.

## 7. Upload the whole project on jupyter hub

Jupyter hub allows to upload only files and not a folder organization. You can upload the project in one step by uploading a **.zip version** of the project on jupyter hub and unzip it with the python code below (you can write this code in a temporary notebook in jupyter hub):

``import zipfile as zf``<br><br>
``files = zf.ZipFile("image_processing_project-master.zip", 'r')``<br>
``files.extractall('.') # Will extract the project at the root of jupyter hub``<br>
``files.close()``<br>

If the package ``zipfile`` is not available install it with the command:<br> ``!pip install zipfile36``
*(run it in a notebook cell)*.

## 8. References

- NGA [website](https://www.nga.gov/)
- ImageNet [website](https://www.image-net.org/index.php)
- Kaggle [website](https://www.kaggle.com/datasets)

## 9. Supervisors
- Matthieu Chancel: matthieu.chancel@gipsa-lab.grenoble-inp.fr
- Mauro Dalla Mura: mauro.dalla-mura@gipsa-lab.grenoble-inp.fr
- Matthieu Muller: matthieu.muller@gipsa-lab.grenoble-inp.fr
- Daniele Picone: daniele.picone@grenoble-inp.fr
