"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students should only call their functions (declared in others files of src/) in it.
"""


import numpy as np
from os import path

# Import the files needed for the reconstruction like:
# from src.file import function_1
from src.demo_reconstructions import get_random_image, get_mosaic


def run_reconstruction(image: np.ndarray, patches_path='patches.npy') -> np.ndarray:
    """_summary_

    Args:
        image (np.ndarray): _description_
        patches_path (str, optional): _description_. Defaults to 'patches.npy'.

    Returns:
        np.ndarray: _description_
    """

    # Performing the reconstruction.
    # TODO

    ######################################## Demo code ########################################
    # Change flag_random to switch from the random reconstruction to the mosaic with one patch.
    flag_random = False

    if flag_random:
        res = get_random_image(image.shape)

    else:
        patch_path = path.join(patches_path)
        res = get_mosaic(image.shape, patch_path)
    ###########################################################################################

    return res


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors : Matthieu Chancel, Mauro Dalla Mura, Matthieu Muller and Daniele Picone