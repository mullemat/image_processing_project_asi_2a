"""A file containing two (pretty useless) reconstructions.
They serve as example of how the project works.
This file should NOT be modified.
"""


import numpy as np

from src.utils import load_patches


def get_random_image(shape: tuple) -> np.ndarray:
    """Generates a random image of the desired shape.

    Args:
        shape (tuple): Desired shape of the output

    Returns:
        np.ndarray: A randomly generated image of the wanted shape.
    """

    rng = np.random.default_rng()

    return rng.integers(0, 256, shape, dtype=np.uint8)


def get_mosaic(shape: tuple, patches_path: str) -> np.ndarray:
    """Fills an image of the desired shape with a patch.

    Args:
        shape (tuple): Desired shape of the output.
        patches_path (str): The path of the file containing the patches. Must be a npy file.

    Returns:
        np.ndarray: An image composed of repetitions of the same small patch.
    """

    patch = load_patches(patches_path, 1)[0]
    ones = np.ones((shape[0] // patch.shape[0], shape[1] // patch.shape[1], 1), dtype=np.uint8)

    return np.kron(ones, patch)


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors : Matthieu Chancel, Mauro Dalla Mura, Matthieu Muller and Daniele Picone